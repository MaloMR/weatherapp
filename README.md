# Weather App

A simple and modern weather application made with [Flutter](https://flutter.dev/) and the [OpenWeatherMap API](https://openweathermap.org/api).\
The app is compatible with both Android and IOS.\
It uses the device's location to determine the appropriate weather data to be deisplayed.

## Running the app

- Clone the repo.
- Add a ``.env`` file to the project's root containing : 
```.env
API_KEY=<YOUR_API_KEY>
```
- Replace ``<YOUR_API_KEY>`` by the API key provided by OpenWeatherMap.
- Make sure o have a device connected.
- Run ``flutter run`` at the project's root.

## Visuals
<!-- 
![](visuals/vannes_rain_dark.gif)
![](visuals/montpellier_sun_light.gif)
![](visuals/saguenay_clouds_dark_reload.gif)

 -->

<img src="visuals/vannes_rain_dark.gif" width=30%>
<img src="visuals/montpellier_sun_light.gif" width=30%>
<img src="visuals/saguenay_clouds_dark_reload.gif" width=30%>
