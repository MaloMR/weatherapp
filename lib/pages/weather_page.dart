import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:weather_app/models/weather_model.dart';
import 'package:weather_app/service/weather_service.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class WeatherPage extends StatefulWidget {
  const WeatherPage({super.key});

  @override
  State<WeatherPage> createState() => _WeatherPageState();
}

class _WeatherPageState extends State<WeatherPage> {
  final _weatherService = WeatherService(dotenv.env['API_KEY']!);
  Weather? _weather;
  bool nightMode = true;

  _fetchWeather() async {
    String cityName = await _weatherService.getCurrentCity();

    try {
      final weather = await _weatherService.getWeather(cityName);
      setState(() {
        _weather = weather;
      });
    } catch (e) {
      print(e);
    }
  }

  String getWeatherAnimation(String? mainCondition) {
    if (mainCondition == null) return 'assets/loading.json';

    switch (mainCondition.toLowerCase()) {
      case 'clouds':
        return 'assets/clouds.json';
      case 'mist':
      case 'smoke':
      case 'haze':
      case 'dust':
      case 'fog':
        return 'assets/mist.json';
      case 'rain':
      case 'drizzle':
      case 'shower rain':
        return 'assets/rain.json';
      case 'thunderstorm':
        return 'assets/thunder.json';
      case 'clear':
        return 'assets/sun.json';
      case 'snow':
        return 'assets/snow.json';
      default:
        return 'assets/loading.json';
    }
  }

  String getTemperature(double? temp) {
    if (temp == null) {
      return "";
    } else {
      return '${temp.round()}°C';
    }
  }

  @override
  void initState() {
    super.initState();
    _fetchWeather();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: nightMode ? const Color(0xff242424) : Colors.white,
      appBar: AppBar(
        toolbarHeight: 50,
        backgroundColor: nightMode ? const Color(0xff242424) : Colors.white,
        actions: [
          IconButton(
            onPressed: () {
              setState(() {
                _weather = null;
                _fetchWeather();
              });
            },
            icon: const Icon(Icons.restart_alt),
            color: nightMode ? const Color(0xff9F9F9F) : Colors.black,
            iconSize: 30,
          ),
        ],
        leading: IconButton(
          onPressed: () {
            setState(() {
              nightMode = !nightMode;
            });
          },
          icon: Icon(nightMode ? Icons.light_mode : Icons.dark_mode),
          color: nightMode ? const Color(0xff9F9F9F) : Colors.black,
          iconSize: 30,
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.location_pin,
                  size: 40,
                  color: _weather == null
                      ? const Color(0x00000000)
                      : (nightMode ? const Color(0xff9F9F9F) : Colors.black),
                ),
                Text(
                  _weather?.cityName ?? "",
                  style: TextStyle(
                    fontSize: 25,
                    color: nightMode ? const Color(0xff626262) : Colors.black,
                  ),
                ),
              ],
            ),
            Lottie.asset(getWeatherAnimation(_weather?.mainCondition)),
            Text(
              getTemperature(_weather?.temperature),
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 30,
                color: nightMode ? const Color(0xff626262) : Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
